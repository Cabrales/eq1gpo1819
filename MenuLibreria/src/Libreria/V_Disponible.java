/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Libreria;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Uriel
 */
public class V_Disponible extends JFrame {
    public V_Disponible()
    {
        marco();
    }
    public void marco(){
        //Aqui se crea la ventana a mostrar
       
        this.setSize(400,350);
        this.setLocation(200,200);
        this.setTitle("Libros Disponibles");
       
       //Paneles que van a ser utilizados
        JPanel panelMayor= new JPanel(new BorderLayout());
        JPanel panelNorte =new JPanel();
        JPanel panelOeste =new JPanel();
        JPanel panelSur =new JPanel();
        
        //Layout Manager a utilizar
        FlowLayout fl1 = new FlowLayout(FlowLayout.CENTER);
        panelNorte.setLayout(fl1);
       
        GridBagLayout gbl1 = new GridBagLayout();
        GridBagConstraints gbc1 = new GridBagConstraints();
        panelOeste.setLayout(gbl1);
        
        FlowLayout fl2 = new FlowLayout();
        panelSur.setLayout(fl2);
        
        //Aqui se crean los componentes par el panel Norte
        JLabel jlbl1Titulo = new JLabel("Registro de Libros Disponibles");
        
        //Aqui se crean los componentes para el panel Oeste
        JLabel jlb1 = new JLabel("Nombre del libro:");
        JTextField tf1 =new JTextField(25);
        JLabel jlb2 = new JLabel("Autor:");
        JTextField tf2=new JTextField(25);
        JLabel jlb3 = new JLabel("Edicion:");
        JTextField tf3=new JTextField(25);
        JLabel jlb4 = new JLabel("Categoria:");
        JTextField tf4=new JTextField(25);
        
        //Aqui creamos los componentes para el panel Sur
        JButton jb1=new JButton();
        JButton jb2=new JButton();
        jb1.setText("Registrar");
        jb2.setText("Cancelar");
        
        //Agrgacion de los componentes a los paneles
        //Agregacion de componentes al panel Norte
        panelNorte.add(jlbl1Titulo);
        
        //Agregacion de componenetes al panel Oeste
        gbc1.gridx=0;
        gbc1.gridy=0;
        gbc1.anchor = GridBagConstraints.CENTER;
        panelOeste.add(jlb1,gbc1);
        gbc1.gridy=1;
        gbc1.gridwidth=5;
        panelOeste.add(tf1,gbc1);
        gbc1.gridy=2;
        gbc1.gridwidth=1;
        panelOeste.add(jlb2,gbc1);
        gbc1.gridy=3;
        gbc1.gridwidth=5;
        panelOeste.add(tf2,gbc1);
        gbc1.gridy=4;
        gbc1.gridwidth=1;
        panelOeste.add(jlb3,gbc1);
        gbc1.gridy=5;
        gbc1.gridwidth=5;
        panelOeste.add(tf3,gbc1);
        gbc1.gridy=6;
        gbc1.gridwidth=1;
        panelOeste.add(jlb4,gbc1);
        gbc1.gridy=7;
        gbc1.gridwidth=5;
        panelOeste.add(tf4,gbc1);
        //Agregacion de componentes al panel Sur
        panelSur.add(jb1);
        panelSur.add(jb2);
        
        //Agregacion al panelMayor
        panelMayor.add(panelNorte,BorderLayout.NORTH);
        panelMayor.add(panelOeste,BorderLayout.CENTER);
        panelMayor.add(panelSur,BorderLayout.SOUTH);
        
        //Asociar a JFrame
        this.add(panelMayor);
        
        this.setVisible(true);
        
    }
}
